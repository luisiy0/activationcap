import React from 'react';
import { connect } from 'react-redux';
import './NavbarDesktop.styles.css';
import MakeInRvaImage from 'assets/images/make_in_rva.png';

import { Row, Col } from 'react-grid-system';
import { Link } from 'react-router-dom'
import UilSearch from '@iconscout/react-unicons/icons/uil-search'
import UilUserCircle from '@iconscout/react-unicons/icons/uil-user-circle'

class NavbarDesktop extends React.Component {
	render () {
		return (
			<Row className="navbar-desktop" justify="between" nogutter>
				<Col md={4}>
					<Link to="/" className="link">
						<img width={"100px"} src={MakeInRvaImage} alt="makeInRVALogo"/>
					</Link>
				</Col>

				<Col md={1} style={{textAlign: 'center'}}>
					<UilSearch size="1.5rem" color="white" />
				</Col>

				{this.props.ui.menus.map(menu =>
					<Col key={menu.name} md={1}>
						<Link to={menu.url} className="link">{menu.name}</Link>
					</Col>
				)}

				<Col md={1}>
					<Link to='/profile' className="link">
						<UilUserCircle size="2rem" color="white" />
					</Link>
				</Col>
			</Row>
		);
	}
}

const mapStateToProps = ({ ui }) => ({ ui });
export default connect(mapStateToProps)(NavbarDesktop);