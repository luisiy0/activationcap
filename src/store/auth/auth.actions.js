import * as persistenceService from 'api/persistence.service';
import * as userService from 'api/users.service';
import * as roleService from 'api/roles.service';

// Synchronous
export const SetUserRole = (userRole) => ({ type: 'set_user_role', payload: userRole });
export const SetUserPhases = (userPhases) => ({ type: 'set_user_phases', payload: userPhases });
export const CompleteLogin = (profile) => ({ type: 'complete_login', payload: profile });
export const AuthError = (error) => ({ type: 'auth_error', payload: error });

// Asynchronous
export const UserRoleChanged = (roleName) => async dispatch => {
	try {
		const role = await roleService.getUserRoleByName(roleName, {withPhases: true});
		dispatch(SetUserRole(role));
		dispatch(SetUserPhases(role ? role.phases : []));
	} catch (e) {
		dispatch(AuthError(e));
	}
};


export const Login = (username, password) => async dispatch => {
	try {
		const response = await userService.login(username, password);
		persistenceService.setItem('auth_profile', JSON.stringify(response.data));
		dispatch(CompleteLogin(response.data));
	} catch (e) {
		dispatch(AuthError(e));
	}
};