module.exports = {
	env: 'local',
	RestUrl: 'http://localhost:1337',
	GraphQlUrl: 'http://localhost:1337/graphql',
};