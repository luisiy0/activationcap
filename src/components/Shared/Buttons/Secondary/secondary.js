import React from "react";
import "./secondary.css";

const Secondary = props => {

    return (
      <button className={`container`}>
        <div className={`divInside`}>
          <p className={`textStyle`}>{props.text}</p>
        </div>
      </button>
    );
};

Secondary.defaultProps = {
  text: "button text"
};

export default Secondary;