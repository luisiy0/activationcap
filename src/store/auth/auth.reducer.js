const initialState = {
	isLoggedIn: false,
	authToken: '',
	userProfile: null,
	userRole: null,
	userPhases: []
};

const authReducer = (state = initialState, action) => {
	switch(action.type) {
		case 'set_user_role':
			return {...state, userRole: action.payload };

		case 'set_user_phases':
			return {...state, userPhases: action.payload };

		case 'complete_login':
			const userProfile = action.payload.user;
			const userRole = userProfile.role;
			const userPhase = userProfile.phase;
			const authToken = action.payload.jwt;
			return {
				...state,
				userRole,
				userPhases: [userPhase],
				authToken,
				userProfile,
				isLoggedIn: true
			};
			
		default:
			return state;
	}
}

export default authReducer;