import NavbarDesktop from './NavBarDesktop/NavbarDesktop';
import NavbarMobile from './NavBarMobile/NavbarMobile';

const Navbar = {
	Desktop: NavbarDesktop,
	Mobile: NavbarMobile
};

export default Navbar;