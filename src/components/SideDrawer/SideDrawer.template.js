import React from 'react';
import MakeInRvaImage from 'assets/images/make_in_rva.png';
import './SideDrawer.styles.css';

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { Menu }from 'components/Shared/Menu';
import { Row, Col } from 'react-grid-system';
import { Link } from 'react-router-dom'
import Footer from '../Footer/Footer';


export default function template() {
	return (
		<SwipeableDrawer
			disableBackdropTransition={!this.iOS}
			disableDiscovery={this.iOS}
			open={this.props.ui.sideDrawerOpened}
			onClose={this.toggleDrawer(false)}
			onOpen={this.toggleDrawer(true)}
			classes={{paper: 'drawer-root'}}
			>
			
			<Row>
				<Col xs={12} style={{textAlign: 'center'}}>
					<Link to="/" className="link">
						<img width={"100px"} src={MakeInRvaImage} alt="makeInRVALogo"/>
					</Link>
				</Col>
			</Row>
			<Row>
				<Col xs={12}>
					<Menu items={this.props.ui.menus}></Menu>
				</Col>
			</Row>
			<Footer Drawer={true}/>
		</SwipeableDrawer>
	);
}