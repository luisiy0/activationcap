import React from 'react';
import template from './Footer.template';

class Footer extends React.Component {

    render() {
        return template.call(this, this.props.Drawer);
    }
}

export default Footer;