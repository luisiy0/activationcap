module.exports = {
	env: 'qa',
	RestUrl: 'http://api.acqa.shockoe.com',
	GraphQlUrl: 'http://api.acqa.shockoe.com/graphql',
};