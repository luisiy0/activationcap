import React from 'react';
import { connect } from 'react-redux';
import { UserRoleChanged } from 'store/auth/auth.actions';
import template from './App.template';

class App extends React.Component {
	routeToRoleMap = {
		'founder': 'founder',
		'investor': 'investor',
		'mentor': 'mentor'
	};

	getRoleNameFromRoute() {
		const route = this.props.location.pathname.split('/')[1];
		return this.routeToRoleMap[route];
	}

	// (1) Determine which user role info. to fetch based on the current url.
	// (2a) If url does not have an user role name: do nothing.
	// (2b) If url specifies an user role: Dispatch redux action to fetch role info. and update store.
	componentDidMount() {
		const roleName = this.getRoleNameFromRoute();

		if (!this.props.auth.isLoggedIn) {
			this.props.UserRoleChanged(roleName);
		} else {
			// Decide what to do here when an already logged in
			// user navigates to another user type(e.g. from /founder to /investor)
		}
	}

	render() {
		return template.call(this);
	}
}

const mapStateToProps = ({ auth }) => ({ auth });
export default connect( mapStateToProps, { UserRoleChanged })(App);