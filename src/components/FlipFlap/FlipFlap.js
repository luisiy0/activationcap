import React from "react";
import "./FlipFlap.styles.css";
import ReactCardFlip from "react-card-flip";

class FlipFlap extends React.Component {
  state = {
    isFlipped: false,
    elapsed: 0
  };

  componentDidMount() {
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillMount() {
    clearInterval(this.timer);
  }

  tick = () => {
    if (this.state.elapsed === 5) {
      this.handleClick(this);
      this.setState({ elapsed: 0 });
    } else {
      this.setState({ elapsed: this.state.elapsed + 1 });
    }
  };

  handleClick = () => {
    this.setState(prevState => ({ isFlipped: !prevState.isFlipped }));
  };

  render() {
    const { textUpFront, textDownFront, textUpBack, textDownBack } = this.props;

    return (
      <div className="flipflap">
        <div className="flipflapContentTop" />
        <div className="flipflapContentBottom" />
        <ReactCardFlip
          isFlipped={this.state.isFlipped}
          flipDirection="vertical"
          className="flipCardFlip"
        >
          <div id="flipFront" key="front">
            <div className="flipFlapTop">
              <p className="flipFlapTextUp">{textUpFront}</p>
            </div>
            <div className="flipFlapBottom">
              <p className="flipFlapTextDown">{textDownFront}</p>
            </div>
          </div>
          <div id="flipBack" key="back">
            <div className="flipFlapTop">
              <p className="flipFlapTextUp">{textUpBack}</p>
            </div>
            <div className="flipFlapBottom">
              <p className="flipFlapTextDown">{textDownBack}</p>
            </div>
          </div>
        </ReactCardFlip>
      </div>
    );
  }
}

FlipFlap.defaultProps = {
  textUpFront: "#1",
  textDownFront: "Beer Destination",
  textUpBack: "12k",
  textDownBack: "Veterans"
};

export default FlipFlap;
