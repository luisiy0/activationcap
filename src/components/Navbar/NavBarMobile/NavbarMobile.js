import React from 'react';
import { connect } from 'react-redux';
import { SideDrawerOpened } from 'store/ui/ui.actions';
import './NavbarMobile.styles.css';

import { Row, Col } from 'react-grid-system';
import { Link } from 'react-router-dom'
import UilBars from '@iconscout/react-unicons/icons/uil-bars'
import UilUserCircle from '@iconscout/react-unicons/icons/uil-user-circle'

class NavbarMobile extends React.Component {
	toggleDrawer = (open) => event => {
		if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		this.props.SideDrawerOpened(open);	
	};

	render() {
		return (
			<Row className="navbar-mobile" justify="between" align="start" nogutter>
				<Col xs={6} onClick={this.toggleDrawer(true)}>
					<UilBars size="2.4rem" color="white" />
				</Col>
				<Col xs={6} style={{textAlign: 'right'}}>
					<Link to="/profile" className="link">
						<UilUserCircle size="2.4rem" color="white" />
					</Link>
				</Col>
			</Row>
		);
	}
}

const mapStateToProps = ({ ui }) => ({ ui });
export default connect(mapStateToProps, { SideDrawerOpened })(NavbarMobile);