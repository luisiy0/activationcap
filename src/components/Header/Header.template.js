import React from 'react';
import './Header.styles.css';
import { Parallax } from 'react-scroll-parallax';
import HeaderContents from './HeaderContents/HeaderContents';
import LinePath from 'components/Shared/LinePath/LinePath';

export default function template() {
	return (
		<div className="headerContainer">
			<div className="linePath">
				<LinePath />	
			</div>
			<div className="header">
				<HeaderContents />
				<Parallax y={[-50, 0]} styleOuter={{position: "absolute", top: "0", width: "100%", zIndex: "-1"}}>
					<div className="headerImage">
						<div className="imageFilter"></div>
					</div>
				</Parallax>
			</div>
			<svg className="header" width="0" height="0">
				<defs>
					<clipPath id="headerClipDesktop" clipPathUnits="objectBoundingBox"
							  transform="scale(0.00043122 0.0014814)">
						<path
							d="M0.999246 0H2318.5V631.968H1730.77C1726.35 631.968 1722.77 635.55 1722.77 639.968V666.993C1722.77 671.414 1719.19 674.997 1714.76 674.993L1165.74 674.507C1161.33 674.503 1157.75 670.922 1157.75 666.507V601.44C1157.75 597.021 1154.17 593.44 1149.75 593.44H597.734C593.316 593.44 589.734 589.858 589.734 585.44V546.406C589.734 541.985 586.149 538.402 581.728 538.406L0.000976562 538.899L0.999246 0Z"></path>
					</clipPath>
					<clipPath id="headerClipMobile" clipPathUnits="objectBoundingBox"
							  transform="scale(0.002666 0.0013422)">
						<path
							d="M-658 -1H925.5V702H698V745L139.993 744.507C135.577 744.503 132 740.923 132 736.507V671.5C132 667.082 128.418 663.5 124 663.5H-437V608.5L-659 609L-658 -1Z"
							fill="#0A0724"></path>
					</clipPath>
					<clipPath id="lineClipDesktop" clipPathUnits="objectBoundingBox"
							  transform="scale(0.000429368 0.00704225)">
						<path
							d="M597.007 55.493L1149.99 55.007C1154.41 55.0032 1158 58.586 1158 63.007V128C1158 132.418 1161.58 136 1166 136H1722C1726.42 136 1730 132.418 1730 128V102C1730 97.5817 1733.58 94 1738 94H2329.5V96H2317.5H1740C1735.58 96 1732 99.5817 1732 104V130C1732 134.418 1728.42 138 1724 138H1164C1159.58 138 1156 134.418 1156 130V64.9998C1156 60.5815 1152.42 56.9998 1148 56.9998H592C589.239 56.9998 587 54.7612 587 51.9998V12C587 6.47715 582.523 2 577 2H0V0C228.382 0 356.327 0.491239 582.941 0.499885C586.255 0.500011 589 3.18629 589 6.5V47.493C589 51.914 592.586 55.4968 597.007 55.493Z"></path>
					</clipPath>
					<clipPath id="lineClipMobile" clipPathUnits="objectBoundingBox" transform="scale(0.002488 0.01205)">
						<path
							d="M0 0.000198364L133.5 1.12149e-05C137.918 5.02111e-06 141.5 3.58173 141.5 8.00001V73C141.5 77.4183 145.082 81 149.5 81H402V83H147.5C143.082 83 139.5 79.4183 139.5 75V9.9998C139.5 5.58152 135.918 1.9998 131.5 1.9998H0V0.000198364Z"></path>
					</clipPath>
				</defs>
			</svg>
		</div>
	);

}