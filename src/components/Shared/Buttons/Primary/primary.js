import React from "react";
import "./primary.css";

const Primary = props => {

    return (
      <button className={`containerPrimary`}>
        <div className={`divInsidePrimary`}>
          <p className={`textStylePrimary`}>{props.text}</p>
        </div>
      </button>
    );
};

Primary.defaultProps = {
  text: "button text"
};

export default Primary;