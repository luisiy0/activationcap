import React from "react";
import "./card.css";
import { Container, Row, Col } from "react-grid-system";
import Tag from "../Buttons/Tag/tag";
import cardBackground from "assets/images/card_background.png";
import pinImage from "assets/icons/pin.png";
import pinImageFilled from "assets/icons/pin_filled.png";

class Card extends React.Component {
  state = { favorite: false };

  setFavorite = () => {
    this.setState({
      favorite: !this.state.favorite
    });
  };

  render() {
    const { img, text, title, subTitle, day, month, showDate } = this.props;

    const { favorite } = this.state;

    let renderDate;
    if (showDate) {
      renderDate = (
        <Row
          style={{
            marginBottom: "-20px"
          }}
        >
          <Col
            style={{
              zIndex: "1"
            }}
            xs={4}
            sm={4}
            md={4}
            offset={{ md: 4, sm: 4, xs:4 }}
          >
            <div className="circleCard">
              <p className="monthClassCard">{month}</p>
              <p className="dayClassCard">{day}</p>
            </div>
          </Col>
        </Row>
      );
    }

    return (
      <Container className="containerCard">
        {renderDate}
        <Row>
          <Col className="containerCardCard" md={12}>
            <Row>
              <Col md={1} offset={{ md: 11 }}>
                <img
                  className="pinCard"
                  src={favorite ? pinImageFilled : pinImage}
                  onClick={this.setFavorite}
                  alt=""
                />
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <p className="titleClassCard">{title}</p>
                <p className="subtitleClassCard">{subTitle}</p>
              </Col>
            </Row>
            <Row>
              <Col md={12} style={{ paddingLeft: "0px" }}>
                <img className="imgCard" src={img} alt="" />
              </Col>
            </Row>
            <Row style={{ marginTop: "16px", marginBottom: "24px" }}>
              <Col md={12}>
                <Col md={4}>
                  <Tag text={text} />
                </Col>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

Card.defaultProps = {
  text: "button",
  img: cardBackground,
  title: "RICHMONDINNO:",
  subTitle: "RVATECH  WOMEN Richmond Convention Center",
  day: "22",
  month: "SEP",
  showDate: true
};

export default Card;
