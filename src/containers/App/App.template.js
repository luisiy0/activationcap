import React from "react";
import { Route } from 'react-router-dom';
import { ParallaxProvider } from 'react-scroll-parallax';
import Home from 'containers/Home/Home';
import JourneyMap from 'containers/JourneyMap/JourneyMap';
import SideDrawer from 'components/SideDrawer/SideDrawer';

export default function template() {
	const curPath = this.props.match.path;
	return (
		<ParallaxProvider>
			<SideDrawer />
			<div>
				<Route exact path={curPath} component={ Home } />
				<Route exact path={`${curPath}/journeymap`} component={ JourneyMap } />
			</div>
		</ParallaxProvider>
	);
};