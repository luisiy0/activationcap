import { gql } from 'apollo-boost';

/**
 * Queries all events belonging to the specified phase ids.
 * $withPhases: whether or not to return phases information.
 */
export const EventsForPhases = gql`
	query EventsForPhases($phaseIds: [ID!]!, $withPhases: Boolean = false) {
		events (where: { phases: { id: $phaseIds }}) {
			id,
			name,
			phases @include(if: $withPhases) { id, name }
		}
	}
`;