import React from "react";
import electricRiver from "assets/images/electric_river.png";
import electricRiverMob1 from "assets/images/electric_river_mobile1.png";
import electricRiverMob2 from "assets/images/electric_river_mobile2.png";
import upArrow from "assets/icons/up_arrow.png";
import "./Home.css";
import Card from "../../components/Shared/Card/card";
import CustomButton from "../../components/Shared/Buttons/Primary/primary";
import Arrow from "../../components/Shared/Buttons/Arrow/arrow";
import Carousel from "../../components/Shared/Carousel/carousel";
import Header from "components/Header/Header";
import { Container, Row, Col, Visible, Hidden } from "react-grid-system";
import { Parallax } from "react-scroll-parallax";
import Footer from "../../components/Footer/Footer";

export default function() {
  return (
    <div>
      <Header />
      <div>
        <Hidden sm xs>
          <Parallax y={[-60, 0]} className="parallaxHome">
            <img className="homeElectricRiver" src={electricRiver} alt="" />
          </Parallax>
        </Hidden>
        <Container>
          <Visible sm xs>
            <Parallax y={[-50, 0]} className="riverMobile1">
              <img
                className="riverMobileImage1"
                src={electricRiverMob1}
                alt=""
              />
            </Parallax>
          </Visible>
          <Row>
            <Col md={12}>
              <h3 className="h3Home">
                <span className="topicLineHome">—</span> GET INVOLVED
              </h3>
              <h2 className="h2Home">Upcoming Events</h2>
            </Col>
          </Row>
          <Visible sm xs>
            <Row>
              <Col sm={12}>
                <Carousel>
                  <Card favorite={true} />
                  <Card />
                  <Card />
                </Carousel>
              </Col>
            </Row>
          </Visible>
          <Hidden sm xs>
            <Row md={12}>
              <Col md={4}>
                <Card favorite={true} />
              </Col>
              <Col md={4}>
                <Card />
              </Col>
              <Col md={4}>
                <Card />
              </Col>
            </Row>
          </Hidden>
          <Row md={12}>
            <Col offset={{ md: 4 }} md={4}>
              <h4 className="h4Home">SHOW ALL EVENTS</h4>
            </Col>
          </Row>
          <Row md={12}>
            <Col offset={{ md: 5 }} md={2}>
              <div className="divArrowHome">
                <Hidden sm xs>
                  <Arrow direction="right" />
                </Hidden>
                <Visible sm xs>
                  <Arrow direction="right" />
                </Visible>
              </div>
            </Col>
          </Row>
          <Row className="rowStartedHome">
            <Col md={12}>
              <h3 className="h3Home">
                <span className="topicLineHome">—</span> GET STARTED
              </h3>
              <h2 className="h2Home">River Guide</h2>
            </Col>
          </Row>
          <Row md={12}>
            <Col md={6} sm={12}>
              <p className="pHome">
                MAKE IN RVA has inspired the city’s drive and creative spirit to
                cultivate the ultimate river guide for our Virginia Founders. We
                know how important it is to Get Involved, Ask for Help, and be
                Ready when the time comes! The more we know about where you are
                on your Journey, the better we can guide you.
              </p>
            </Col>
            <Col
              className="divJourneyHome"
              offset={{ md: 1, sm: 1 }}
              md={5}
              sm={12}
            >
              <CustomButton text="JOURNEY MAP" />
            </Col>
          </Row>
          <Row className="rowArticlesHome">
            <Col md={12}>
              <h3 className="h3Home">
                <span className="topicLineHome">—</span> STARTUP STORIES
              </h3>
              <h2 className="h2Home">SHOW ALL ARTICLES &amp; NEWS</h2>
            </Col>
          </Row>
          <Visible sm xs>
            <Row>
              <Col sm={12}>
                <Carousel>
                  <Card favorite={true} />
                  <Card />
                </Carousel>
              </Col>
            </Row>
          </Visible>
          <Hidden sm xs>
            <Row md={12}>
              <Col md={4}>
                <Card showDate={false} />
              </Col>
              <Col md={4}>
                <Card showDate={false} />
              </Col>
              <Col md={4}>
                <Card showDate={false} />
              </Col>
            </Row>
          </Hidden>
          <Row md={12}>
            <Col offset={{ md: 4 }} md={4}>
              <h4 className="h4Home">SHOW ALL ARTICLES &amp; NEWS</h4>
            </Col>
          </Row>
          <Row md={12}>
            <Col offset={{ md: 5 }} md={2}>
              <div className="divArrowHome">
                <Hidden sm xs>
                  <Arrow direction="right" />
                </Hidden>
                <Visible sm xs>
                  <Arrow direction="right" />
                </Visible>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="divStartUpHome">
        <Container>
          <Row md={12}>
            <Col
              style={{
                zIndex: "1"
              }}
              offset={{ md: 5 }}
              md={2}
            >
              <div className="divMidleStartupHome">
                <button className="circleStartUpHome">
                  <img className="imgStartUpHome" src={upArrow} alt="" />
                </button>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <div className="homeStartupSection">
                <hr className="hrStartUpHome" />
                <h2 className="h2StartUpHome">
                  <span>
                    <span className="topicLineHome">RVA</span> STARTUP RESOURCES
                  </span>
                </h2>
                <hr className="hrStartUpHome" />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer />
    </div>
  );
}
