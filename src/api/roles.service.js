import { strapi } from './strapi.client';
import { UserRoles } from './queries/role.queries';

export async function getUserRoles(variables) {
	return strapi.graphql.query({
		query: UserRoles,
		variables
	});
}

export async function getUserRoleByName(roleName, variables) {
	try {
		const resp = await getUserRoles(variables);
		const role = resp.data.roles.find(role => role.name.toLowerCase() === roleName);
		return Promise.resolve(role);
	} catch(e) {
		return Promise.reject(e);
	}
}

export async function getUserRoleById(roleId) {
	return strapi.rest.get(`/users-permissions/roles/${roleId}`);
}
