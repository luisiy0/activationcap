import React from "react";
import "./Footer.styles.css";
import { Container, Row, Col, Visible, Hidden } from "react-grid-system";
import Logo from "assets/images/activation_capital_logo.png";
import UilFacebook from "@iconscout/react-unicons/icons/uil-facebook-f";
import UilLinkedin from "@iconscout/react-unicons/icons/uil-linkedin";
import UilTwitter from "@iconscout/react-unicons/icons/uil-twitter";

export default function template(Drawer) {
  let footerLinks;
  if (!Drawer) {
    footerLinks = (
      <Col md={10} sm={12} xs={12}>
        <div className="footerDivLinks">
          <Row>
            <Col>
              <a className="footerA" href="">
                Founders
              </a>
            </Col>
            <Col>
              <a className="footerA" href="">
                Mentors
              </a>
            </Col>
            <Col>
              <a className="footerA" href="">
                Investors
              </a>
            </Col>
            <Col className="footerMarginLinks" offset={{ sm: 2, xs: 2, md:0 }}>
              <a className="footerA" href="">
                AboutUs
              </a>
            </Col>
            <Col className="footerMarginLinks">
              <a className="footerA" href="">
                Contact
              </a>
            </Col>
          </Row>
        </div>
      </Col>
    );
  }

  return (
    <div className="footerContainer">
      <Container>
        <Row>
          <Hidden xs sm>
            <Col md={2}>
              <p className="footerLogoText">Powered By</p>
              <img className="footerLogoImg" src={Logo} alt="" />
            </Col>
          </Hidden>
          {footerLinks}
          <Visible xs sm>
            <Row className="footerMarginLinks">
              <Col offset={{ sm: 1, xs: 1 }}>
                <p className="footerLogoText">Powered By</p>
              </Col>
              <Col>
                <img className="footerLogoImg" src={Logo} alt="" />
              </Col>
            </Row>
          </Visible>
        </Row>
        <Row>
          <Col>
            <hr className="footerHr" />
          </Col>
        </Row>
        <Row className="footerRow2">
          <Col md={6} sm={12} xs={12}>
            <div className="footerDivLinksB">
              <Row>
                <Col>
                  <a className="footerB" href="">
                    Terms
                  </a>
                </Col>
                <Col>
                  <a className="footerB" href="">
                    Events
                  </a>
                </Col>
                <Col>
                  <a className="footerB" href="">
                    Resources
                  </a>
                </Col>
              </Row>
            </div>
          </Col>
          <Col md={6} sm={12} xs={12}>
            <div className="footerDivLinks">
              <Row className="footerMarginLinks">
                <Col offset={{ sm: 4, xs: 4 }} md={1} sm={1} xs={1}>
                  <UilTwitter
                    className="footerIcons"
                    size="16"
                    color="#ffffff"
                  />
                </Col>
                <Col md={1} sm={1} xs={1}>
                  <UilLinkedin
                    className="footerIcons"
                    size="16"
                    color="#ffffff"
                  />
                </Col>
                <Col md={1} sm={1} xs={1}>
                  <UilFacebook
                    className="footerIcons"
                    size="16"
                    color="#ffffff"
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
