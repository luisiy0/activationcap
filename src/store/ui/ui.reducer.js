const initialState = {
	sideDrawerOpened: false,
	menus: [
		{ name: 'Founders', url: '/founder' },
		{ name: 'Mentors', url: '/mentors' },
		{ name: 'Investors', url: '/investors' },
		{ name: 'About Us', url: '/founder' },
		{ name: 'Contact', url: '/founder' }
	]
};

const uiReducer = (state = initialState, action) => {
	switch(action.type) {
		case 'side_drawer_opened':
			return { ...state, sideDrawerOpened: action.payload };

		default:
			return state;
	}
}

export default uiReducer;