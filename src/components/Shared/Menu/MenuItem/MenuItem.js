import React from 'react';
import './MenuItem.styles.css';
import { Link } from 'react-router-dom'

export default function MenuItem(props) {
	return (
		<div className="menuItem">
			<Link to={props.url} className="link">
				{props.name}
				{props.children}
			</Link>
		</div>
	);
}

MenuItem.defaultProps = {
	name: '',
	url: ''
};