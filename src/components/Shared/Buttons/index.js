import Arrow from './Arrow/arrow'
import Primary from './Primary/primary'
import Secondary from './Secondary/secondary'
import Tag from './Tag/tag'


const Buttons = {
	Arrow,
	Primary,
	Secondary,
	Tag
};

export default Buttons;