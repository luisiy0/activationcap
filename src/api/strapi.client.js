import axios from 'axios';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import config from '../config';
import store from 'store';

/**
 * Setup Strapi REST Axios client
 */
const strapiRest = axios.create({ baseURL: config.RestUrl });
strapiRest.interceptors.request.use( config => {
	config.headers.Authorization = getAuthToken();
	return config;
}, error => Promise.reject(error));

/**
 * Setup Strapi GraphQL Apollo Client
 */
const httpLink = createHttpLink({ uri: config.GraphQlUrl });
const authLink = setContext((_, { headers }) => {
	return {
		headers: {
			...headers,
			Authorization: getAuthToken()
		}
	}
});
const strapiGraphQl = new ApolloClient({
	link: authLink.concat(httpLink),
	cache: new InMemoryCache()
});

/**
 * Exports all available HTTP clients
 */
export const strapi = {
	rest: strapiRest,
	graphql: strapiGraphQl
}


function getAuthToken() {
	// const token = localStorage.getItem('authToken'); // GET THIS FROM REDUX STORE
	const token = store.getState().auth.authToken;
	return token.length ? `Bearer ${token}` : '';
}