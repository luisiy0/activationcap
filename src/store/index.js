import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import authReducer from './auth/auth.reducer';
import uiReducer from './ui/ui.reducer';

const appReducer = combineReducers({
	auth: authReducer,
	ui: uiReducer
});

const store = createStore(appReducer, applyMiddleware(thunk));
export default store;
