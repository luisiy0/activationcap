import React from 'react';
import './HeaderContents.styles.css';
import { Container, Row, Col, Visible, Hidden } from 'react-grid-system';
import Navbar from 'components/Navbar';
import Buttons from 'components/Shared/Buttons/';
import FlipFlap from 'components/FlipFlap/FlipFlap';

export default function template() {
	return (
		<Container className="headerContent" fluid={true} sm>
			<Row>
				<Col xs={24}>
					<Hidden xs sm><Navbar.Desktop /></Hidden>
					<Visible xs sm><Navbar.Mobile /></Visible>
				</Col>
			</Row>
			<Row>
				<Col className="headerText" xs={24} md={8}>
					<h1 className="headerTitle">{this.headerText}</h1>
				</Col>
			</Row>
			<Row >
				<Col className="headerText" xs={24} md={5}>
					<h3 className="headerSubtitle">{this.headerSubText}</h3>
				</Col>
			</Row>
			<Row>
				<Col xs={24} md={6} className="headerButton">
					<Buttons.Primary text="JUMP IN" />
				</Col>
			</Row>
			
				<div className="flipflop">
					<FlipFlap />
				</div>
			
			{/* <Visible xs sm>
				<Row justify="center" align="center">
					<Col xs={24}>
						<FlipFlap />
					</Col>
				</Row>
			</Visible> */}
		</Container>	
	);
}