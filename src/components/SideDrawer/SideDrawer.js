import React from 'react';
import { connect } from 'react-redux';
import { SideDrawerOpened } from 'store/ui/ui.actions';
import template from './SideDrawer.template';

class SideDrawer extends React.Component {
	iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

	toggleDrawer = (open) => event => {
		if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		this.props.SideDrawerOpened(open);	
	};

	render() {
		return template.call(this);
	}
}

const mapStateToProps = ({ ui }) => ({ ui });
export default connect(mapStateToProps, { SideDrawerOpened })(SideDrawer);