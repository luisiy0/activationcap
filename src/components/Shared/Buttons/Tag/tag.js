import React from "react";
import "./tag.css";

const Tag = props => {

  const { text } = props;

  return (
    <button className="containerTag">
      <p className="textTag">{text}</p>
    </button>
  );
};

Tag.defaultProps = {
  text: "text"
};

export default Tag;
