import React from 'react';
import template from './HeaderContents.template';

class HeaderContents extends React.Component {
	headerText = 'The Front Door To Start (Up)';
	headerSubText = `Our region, is on the rise.
	Stories about our innovative spirit and entrepreneurial success are spreading,
	with accolades coming in from all corners of our country.`;

	render() {
		return template.call(this);
	}
}

export default HeaderContents;