import React from 'react';
import MenuItem from './MenuItem/MenuItem';
import './Menu.styles.css'

export default class Menu extends React.Component {
	render() {
		return (
			<div className="menu">
				{this.props.items.map(item =>
					<MenuItem key={item.name} url={item.url} name={item.name}/>
				)}
				{this.props.children}
			</div>
		);
	}
}

Menu.defaultProps = {
	items: []
};