import React from "react";
import downArrow from "assets/icons/down_arrow.png";
import upArrow from "assets/icons/up_arrow.png";
import leftArrow from "assets/icons/left_arrow.png";
import rightArrow from "assets/icons/right_arrow.png";
import "./arrow.css";


const Arrow = props => {

  const { direction } = props;
  let src;  
  switch (direction) {
    case "down":
      src = downArrow;
      break;
    case "up":
      src = upArrow;
      break;
    case "left":
      src = leftArrow;
      break;
    case "right":
      src = rightArrow;
      break;
    default :
      src = downArrow;
      break;
  }

  return (
    <button className={`containerArrow`}>
      <img className="imgArrow" src={src} alt="" />
    </button>
  );
};

Arrow.defaultProps = {      
  direction: "down"
};

export default Arrow;
