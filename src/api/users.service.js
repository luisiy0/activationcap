import { strapi } from './strapi.client';

export async function login(username, password) {
	const data = {
		identifier: username,
		password: password
	};

	return strapi.rest.post(`/auth/local`, data);
};