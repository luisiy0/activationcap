let config;
switch (process.env.REACT_APP_STAGE) {
  case "prod":
    config = require('./config.prod');
    break;
  case "qa":
    config = require('./config.qa');
    break;
  case "dev":
    config = require('./config.dev');
    break;
  default:
    config = require('./config.local');
}

export default {
  // Add common config values here
  ...config
};
