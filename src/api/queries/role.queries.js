import { gql } from 'apollo-boost';

/**
 * Get all user roles
 * $withPhases: whether or not to return phases information.
 */
export const UserRoles = gql`
	query UserRoles($withPhases: Boolean = false) {
		roles {
			id,
			name,
			description,
			type,
			phases @include(if: $withPhases) { id, name }
		}
	}
`;