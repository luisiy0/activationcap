import React from 'react';
import config from "config";
import * as pkgJson from '../../../package.json';

const styles = {
	fontSize: '12px',
	position: 'fixed',
	bottom: '0px',
	right: '0px'
};

export default function()  {
	return <span style={styles}><b>Version:</b> {config.env} {pkgJson.version}</span>;
}