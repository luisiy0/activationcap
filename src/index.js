import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import store from 'store';
import { strapi } from 'api/strapi.client';

import { BrowserRouter, Route } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import App from 'containers/App/App';
import Version from 'components/Version/Version';

ReactDOM.render(
	<ApolloProvider client={strapi.graphql}>
		<Provider store={store}>
			<BrowserRouter>
				<div>
					<Route exact path="/" component={App} />
					<Route path="/founder" component={App} />
					<Route path="/investor" component={App} />
					<Route path="/mentor" component={App} />
				</div>
			</BrowserRouter>
		</Provider>
		<Version />
	</ApolloProvider>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
