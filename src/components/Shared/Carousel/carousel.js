import React from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";

const Carousel = (props) => {

  console.log("childre ",props.children);

  return (
    <AliceCarousel mouseDragEnabled buttonsDisabled infinite={false}>    
      {props.children}
    </AliceCarousel>
  );
};

export default Carousel;
