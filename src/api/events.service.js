import { strapi } from './strapi.client';
import { EventsForPhases } from './queries/event.queries';

export async function getEventsForPhases(phaseIds, withPhases) {
	return strapi.graphql.query({
		query: EventsForPhases,
		variables: {
			phaseIds,
			withPhases
		}
	});
}

export async function getEvent(eventId) {
	return strapi.rest.get(`/events/${eventId}`);
}
