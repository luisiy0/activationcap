export function setItem(key, item) {
	return localStorage.setItem(key, item);
}

export function getItem(key) {
	return localStorage.getItem(key);
}

export function clearAll() {
	return localStorage.clearAll();
}